﻿namespace S4sme.Init
{
    using System.Web.Mvc;

    using NHibernate;

    using S4sme.NHibernateProvider;

    using SharpLite.Domain.DataInterfaces;
    using SharpLite.NHibernateProvider;

    using StructureMap;

    /// <summary>
    /// Dependency resolver initializer
    /// </summary>
    public class DependencyResolverInitializer
    {
        /// <summary>
        /// Dependency initializer
        /// </summary>
        public static void Initialize()
        {
            var container = new Container(
                x =>
                    {
                        x.For<ISessionFactory>().Singleton().Use(
                            () => NHibernateInitializer.Initialize().BuildSessionFactory());
                        x.For<IEntityDuplicateChecker>().Use<EntityDuplicateChecker>();
                        x.For(typeof(IRepository<>)).Use(typeof(Repository<>));
                        x.For(typeof(IRepositoryWithTypedId<,>)).Use(typeof(RepositoryWithTypedId<,>));
                    });

            DependencyResolver.SetResolver(new StructureMapDependencyResolver(container));
        }
    }
}