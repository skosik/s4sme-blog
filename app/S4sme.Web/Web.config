﻿<?xml version="1.0" encoding="utf-8"?>
<!--
  For more information on how to configure your ASP.NET application, please visit
  http://go.microsoft.com/fwlink/?LinkId=152368
  -->
<configuration>
  <configSections>
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, log4net" requirePermission="false" />
    <sectionGroup name="elmah">
      <section name="security" requirePermission="false" type="Elmah.SecuritySectionHandler, Elmah" />
      <section name="errorLog" requirePermission="false" type="Elmah.ErrorLogSectionHandler, Elmah" />
      <section name="errorMail" requirePermission="false" type="Elmah.ErrorMailSectionHandler, Elmah" />
      <section name="errorFilter" requirePermission="false" type="Elmah.ErrorFilterSectionHandler, Elmah" />
    </sectionGroup>
  </configSections>
  <connectionStrings>
    <add name="S4smeConnectionString" connectionString="Data Source=|DataDirectory|s4sme-blog.sdf;Persist Security Info=False;" providerName="System.Data.SqlServerCe.4.0" />
  </connectionStrings>
  <appSettings>
    <add key="webpages:Version" value="1.0.0.0" />
    <add key="ClientValidationEnabled" value="true" />
    <add key="UnobtrusiveJavaScriptEnabled" value="true" />
    <add key="loginUrl" value="~/Authentication/Login" />
    <add key="RouteDebugger:Enabled" value="false" />
  </appSettings>
  <log4net>
    <appender name="LogToFile" type="log4net.Appender.FileAppender">
      <file value="../../logs/S4sme.Web.log" />
      <appendToFile value="false" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%d [%t] %-5l - %m%n%n" />
      </layout>
    </appender>
    <root>
      <!-- Value of priority may be ALL, DEBUG, INFO, WARN, ERROR, FATAL, OFF -->
      <priority value="INFO" />
      <appender-ref ref="LogToFile" />
    </root>
  </log4net>
  <elmah>
    <security allowRemoteAccess="1" />
    <errorLog type="Elmah.XmlFileErrorLog, Elmah" logPath="~/ElmahLogs" />
    <!--<errorFilter>
      <test>
        <equal binding="HttpStatusCode" value="404" type="Int32" />
      </test>
    </errorFilter>-->
  </elmah>
  <system.web>
    <compilation debug="false" targetFramework="4.0">
      <assemblies>
        <add assembly="System.Web.Abstractions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.Helpers, Version=1.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.Routing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.Mvc, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.WebPages, Version=1.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
      </assemblies>
    </compilation>
    <!-- set to mode="On" for production and Off for localhost -->
    <customErrors mode="On" defaultRedirect="/Error/Error">      
      <error statusCode="404" redirect="/Error/NotFound" />
    </customErrors>
    <authentication mode="Forms">
      <forms loginUrl="~/Authentication/Login" name=".S4smeAuthentication" protection="All" defaultUrl="/" timeout="60"></forms>
    </authentication>
    <pages>
      <namespaces>
        <add namespace="System.Web.Helpers" />
        <add namespace="System.Web.Mvc" />
        <add namespace="System.Web.Mvc.Ajax" />
        <add namespace="System.Web.Mvc.Html" />
        <add namespace="System.Web.Routing" />
        <add namespace="System.Web.WebPages" />
        <add namespace="S4sme.Web.Resources" />
      </namespaces>
    </pages>
    <httpModules>
      <add name="SessionPerRequestModule" type="SharpLite.NHibernateProvider.Web.SessionPerRequestModule, SharpLite.NHibernateProvider" />
      <add name="ErrorLog" type="Elmah.ErrorLogModule, Elmah" />
      <add name="ErrorMail" type="Elmah.ErrorMailModule, Elmah" />
      <add name="ErrorFilter" type="Elmah.ErrorFilterModule, Elmah" />
    </httpModules>
    <httpHandlers>
      <add verb="POST,GET,HEAD" path="elmah.axd" type="Elmah.ErrorLogPageFactory, Elmah" />
    </httpHandlers>
  </system.web>
  <system.webServer>
    <validation validateIntegratedModeConfiguration="false" />
    <modules runAllManagedModulesForAllRequests="true">
      <add name="SessionPerRequestModule" type="SharpLite.NHibernateProvider.Web.SessionPerRequestModule, SharpLite.NHibernateProvider" />
      <add name="ErrorLog" type="Elmah.ErrorLogModule, Elmah" preCondition="managedHandler" />
      <add name="ErrorMail" type="Elmah.ErrorMailModule, Elmah" preCondition="managedHandler" />
      <add name="ErrorFilter" type="Elmah.ErrorFilterModule, Elmah" preCondition="managedHandler" />
    </modules>
    <handlers>
      <add name="Elmah" path="elmah.axd" verb="POST,GET,HEAD" type="Elmah.ErrorLogPageFactory, Elmah" preCondition="integratedMode" />
    </handlers>
  </system.webServer>
  <system.data>
    <DbProviderFactories>
      <remove invariant="System.Data.SqlServerCe.4.0" />
      <add name="Microsoft SQL Server Compact Data Provider 4.0" invariant="System.Data.SqlServerCe.4.0" description=".NET Framework Data Provider for Microsoft SQL Server Compact" type="System.Data.SqlServerCe.SqlCeProviderFactory, System.Data.SqlServerCe, Version=4.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" />
    </DbProviderFactories>
  </system.data>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-2.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Data.SQLite" publicKeyToken="db937bc2d44ff139" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-1.0.66.0" newVersion="1.0.66.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
</configuration>