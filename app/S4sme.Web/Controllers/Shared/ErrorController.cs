﻿namespace S4sme.Web.Controllers.Shared
{
    using System.Web.Mvc;

    public class ErrorController : Controller
    {
        public ActionResult Error()
        {
            return View();
        }

        public ActionResult NotFound()
        {
            return View();
        }
    }
}
