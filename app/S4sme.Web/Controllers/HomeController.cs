﻿namespace S4sme.Web.Controllers
{
    using System.Net.Mail;
    using System.Web.Mvc;

    using S4sme.Core.Services;
    using S4sme.Tasks.ViewModels;

    [HandleError]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
           return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Contact(CreateContactViewModel contactForm)
        {
            if (ModelState.IsValid)
            {
                EmailService.SendEmail(
                    new MailAddress(contactForm.Email),
                    "Site",
                    contactForm.Subject,
                    contactForm.Message,
                    contactForm.Name);

                return RedirectToAction("Message", "Home", new { message = Resources.Resource_Blog.ContactMessageRecieved, returnUrl = Url.Action("Contact", "Home") });
            }

            return this.View();
        }

        public ActionResult Message(string message, string returnUrl)
        {
            ViewBag.Message = message;
            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Solutions()
        {
            return this.View();
        }
    }
}
