﻿namespace S4sme.Web.Areas.Blog
{
    using System.Web.Mvc;

    using S4sme.Web.Infrastructure.Globalisation;

    public class BlogAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Blog";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            /*var aboutRouteUrl = context.MapRoute(
                "About", "Blog/{action}", new { controller = "About" }, new[] { "S4sme.Web.Areas.Blog.Controllers.About" });*/

            var postDetailsRouteUrl = context.MapRoute(
                "Post",
                "Blog/Post/{slug}",
                new { controller = "Posts", action = "Details" });

            var postsInCategoryRouteUrl = context.MapRoute(
                "CategoryPosts", "Blog/{controller}/Category/{category}", new { action = "PostsInCategory" });

            var postsInTagRouteUrl = context.MapRoute(
                "TagPosts", "Blog/{controller}/Tag/{tag}", new { action = "PostsInTag" });

            var blogDefaultRouteUrl = context.MapRoute(
                "Blog_default",
                "Blog/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional });

            var globalisedRoute = new GlobalisedRoute(blogDefaultRouteUrl.Url, blogDefaultRouteUrl.Defaults, blogDefaultRouteUrl.DataTokens);

            var globalisedPostDetailsRoute = new GlobalisedRoute(postDetailsRouteUrl.Url, postDetailsRouteUrl.Defaults, postDetailsRouteUrl.DataTokens);

            var globalisedPostsInCategoryRoute = new GlobalisedRoute(postsInCategoryRouteUrl.Url, postsInCategoryRouteUrl.Defaults, postsInCategoryRouteUrl.DataTokens);

            var globalisedPostsInTagRoute = new GlobalisedRoute(postsInTagRouteUrl.Url, postsInTagRouteUrl.Defaults, postsInTagRouteUrl.DataTokens);

            context.Routes.Remove(postDetailsRouteUrl);
            context.Routes.Remove(postsInCategoryRouteUrl);
            context.Routes.Remove(postsInTagRouteUrl);
            context.Routes.Remove(blogDefaultRouteUrl);

            context.Routes.Add(globalisedPostDetailsRoute);
            context.Routes.Add(globalisedPostsInCategoryRoute);
            context.Routes.Add(globalisedPostsInTagRoute);
            context.Routes.Add(globalisedRoute);
            
            context.Routes.Add(postDetailsRouteUrl);
            context.Routes.Add(postsInCategoryRouteUrl);
            context.Routes.Add(postsInTagRouteUrl);
            context.Routes.Add(blogDefaultRouteUrl);
        }
    }
}
