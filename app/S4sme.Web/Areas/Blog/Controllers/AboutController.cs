﻿namespace S4sme.Web.Areas.Blog.Controllers
{
    using System.Net.Mail;
    using System.Web.Mvc;

    using S4sme.Core.Services;
    using S4sme.Tasks.ViewModels;

    public class AboutController : Controller
    {
        //
        // GET: /Blog/About/

        public ActionResult Profile()
        {
            return View();
        }

        public ActionResult Projects()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        /* NOTE: [ValidateAntiForgeryToken] was throwing the exception: System.Web.Mvc.HttpAntiForgeryException. A required anti-forgery token was not supplied or was invalid. */
        public ActionResult Contact(CreateContactViewModel contactForm)
        {
            if (ModelState.IsValid)
            {
                EmailService.SendEmail(
                    new MailAddress(contactForm.Email),
                    "Blog",
                    contactForm.Subject,
                    contactForm.Message,
                    contactForm.Name);

                return RedirectToAction("Message", "Notification", new { message = Resources.Resource_Blog.ContactMessageRecieved, returnUrl = Url.Action("Contact", "About") });
            }

            return this.View();
        }

        public ActionResult Cv()
        {
            return View();
        }

    }
}
