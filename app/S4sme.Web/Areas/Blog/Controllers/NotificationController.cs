﻿namespace S4sme.Web.Areas.Blog.Controllers
{
    using System.Web.Mvc;

    public class NotificationController : Controller
    {
        public ActionResult Message(string message, string returnUrl)
        {
            ViewBag.Message = message;
            ViewBag.ReturnUrl = returnUrl;

            return View();
        }
    }
}
