﻿namespace S4sme.Web.Areas.Blog.Controllers
{
    using System.Linq;
    using System.Web.Mvc;

    using S4sme.Domain;

    using SharpLite.Domain.DataInterfaces;

    public class TagController : Controller
    {
        private readonly IRepository<Tag> tagRepository;

        private readonly IRepository<Post> postRepository;


        public TagController(IRepository<Tag> tagRepository, IRepository<Post> postRepository)
        {
            this.tagRepository = tagRepository;
            this.postRepository = postRepository;
        }

        // will be used as widget
        public ActionResult List()
        {
            ViewBag.TotalPosts = this.postRepository.GetAll().Count(); // TODO: should filter by IsPublished
            return this.PartialView(this.tagRepository.GetAll().OrderBy(t => t.Name));
        }
    }
}
