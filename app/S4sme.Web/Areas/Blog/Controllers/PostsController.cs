﻿namespace S4sme.Web.Areas.Blog.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;

    using S4sme.Core.Configuration;
    using S4sme.Domain;
    using S4sme.Domain.Queries.Post;
    using S4sme.Tasks.Blog.ViewModels;

    using SharpLite.Domain.DataInterfaces;

    [HandleError]
    public class PostsController : Controller
    {
        private readonly IRepository<Post> postRepository;

        public PostsController(IRepository<Post> postRepository)
        {
            this.postRepository = postRepository;
        }

        // GET: /Blog/Post/
        public ActionResult Index(int? page)
        {
            var skip = (page.GetValueOrDefault(1) - 1) * Settings.PostsPerPage;

            int postsCount;
            var recentPosts = this.postRepository.GetAll().FindPagedRecentPosts(skip, Settings.PostsPerPage, out postsCount);
            
            var model = new PostIndexViewModel
            {
                CurrentPage = page.GetValueOrDefault(1),
                RecentPosts = recentPosts,
                TotalPages = (int)Math.Ceiling(postsCount / (double)Settings.PostsPerPage)
            };

            ViewBag.Title = Resources.Resource_Blog.BlogName + " - " + Resources.Resource_Blog.BlogDescription;
            ViewBag.Description = Resources.Resource_Blog.BlogDescription;

            if (page.HasValue)
            {
                ViewBag.Title += " - Page " + page.Value;
                ViewBag.Description += " - Page " + page.Value;
            }

            return View(model);
        }

        public virtual ActionResult PostsInTag(string tag, int? page)
        {
            var skip = (page.GetValueOrDefault(1) - 1) * Settings.PostsPerPage;

            int postsCount;
            var recentPosts = this.postRepository.GetAll().FindPagedRecentPosts(skip, Settings.PostsPerPage, out postsCount, tag);
            var model = new PostIndexViewModel
            {
                CurrentPage = page.GetValueOrDefault(1),
                RecentPosts = recentPosts,
                TotalPages = (int)Math.Ceiling(postsCount / (double)Settings.PostsPerPage)
            };

            ViewBag.Title = "All posts tagged '" + tag + "'";
            ViewBag.Description = Resources.Resource_Blog.BlogDescription;

            if (page.HasValue)
            {
                ViewBag.Title += " - Page " + page.Value;
                ViewBag.Description += " - Page " + page.Value;
            }

            return View("Index", model);
        }

        public virtual ActionResult PostsInCategory(string category, int? page)
        {
            var skip = (page.GetValueOrDefault(1) - 1) * Settings.PostsPerPage;

            int postsCount;
            var recentPosts = this.postRepository.GetAll().FindPagedRecentPosts(skip, Settings.PostsPerPage, out postsCount, null, category);
            var model = new PostIndexViewModel
            {
                CurrentPage = page.GetValueOrDefault(1),
                RecentPosts = recentPosts,
                TotalPages = (int)Math.Ceiling(postsCount / (double)Settings.PostsPerPage)
            };

            ViewBag.Title = category;
            ViewBag.Description = Resources.Resource_Blog.BlogDescription;

            if (page.HasValue)
            {
                ViewBag.Title += " - Page " + page.Value;
                ViewBag.Description += " - Page " + page.Value;
            }

            return View("Index", model);
        }

        public ActionResult Details(string slug)
        {
            var post = this.postRepository.GetAll().SingleOrDefault(x => x.Slug == slug);

            if (post == null)
            {
                return this.RedirectToAction("NotFound", "Error", new { area = string.Empty });
            }

            ViewBag.Title = post.Title;
            ViewBag.Keywords = string.Join(",", post.Tags.Select(x => x.Name).Concat(post.Categories.Select(x => x.Name)));
            ViewBag.Description = post.Description;

            return this.View(post);
        }

        /// <summary>
        /// Used for displaying post details after new comment added 
        /// </summary>
        /// <param name="id">id of post</param>
        /// <returns>Action result to Details view</returns>
        public ActionResult DetailsById(int id)
        {
            var post = this.postRepository.Get(id);

            if (post == null)
            {
                return this.RedirectToAction("NotFound", "Error", new { area = string.Empty });
            }

            ViewBag.Title = post.Title;
            ViewBag.Keywords = string.Join(",", post.Tags.Select(x => x.Name).Concat(post.Categories.Select(x => x.Name)));
            ViewBag.Description = post.Description;

            return this.View("Details", post);
        }

        public JsonResult MostRecentPosts()
        {
            int postsCount;
            var recentPosts = this.postRepository.GetAll().FindPagedRecentPosts(0, 2, out postsCount).GetRecentPostsSummary();

            var result = Json(recentPosts, JsonRequestBehavior.AllowGet);

            return result;
        }
    }
}
