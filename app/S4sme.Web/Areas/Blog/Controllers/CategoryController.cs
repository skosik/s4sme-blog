﻿namespace S4sme.Web.Areas.Blog.Controllers
{
    using System.Linq;
    using System.Web.Mvc;

    using S4sme.Domain;

    using SharpLite.Domain.DataInterfaces;

    public class CategoryController : Controller
    {
        private readonly IRepository<Category> categoryRepository;

        public CategoryController(IRepository<Category> categoryRepository)
        {
            this.categoryRepository = categoryRepository;
        }

        // will be used as widget
        public ActionResult List()
        {
            return this.PartialView(this.categoryRepository.GetAll().OrderBy(pc => pc.Name));
        }
    }
}
