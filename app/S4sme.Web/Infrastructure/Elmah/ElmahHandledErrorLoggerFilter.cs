﻿namespace S4sme.Web.Infrastructure.Elmah
{
    using System.Web.Mvc;

    using global::Elmah;

    /// <summary>
    /// Exception filter which logs handled exceptions before ASP.NET MVC swallows them.
    /// <see cref="http://ivanz.com/2011/05/08/asp-net-mvc-magical-error-logging-with-elmah/"/> 
    /// </summary>
    public class ElmahHandledErrorLoggerFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            // Long only handled exceptions, because all other will be caught by ELMAH anyway.            
            if (context.ExceptionHandled)
            {
                ErrorSignal.FromCurrentContext().Raise(context.Exception);
            }
        }
    }
}