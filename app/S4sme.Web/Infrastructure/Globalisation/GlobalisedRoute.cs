﻿namespace S4sme.Web.Infrastructure.Globalisation
{
    using System.Web.Routing;

    /// <summary>
    /// Thanks to Keith Barrow (http://www.codeproject.com/Articles/207602/Creating-a-Bilingual-ASP-NET-MVC3-Application-Part/?fid=1631764&df=90&mpp=10&noise=3&prof=True&sort=Position&view=Quick&fr=11#xx0xx)
    /// </summary>
    public class GlobalisedRoute : Route
    {
        public const string CultureKey = "culture";

        /// <summary>
        ///    Initializes a new instance of the System.Web.Routing.Route class, by using
        ///    the specified URL pattern, default parameter values, and handler class.
        /// </summary>
        /// <param name="globalisedUrl">The URL pattern for the route, without the culture</param>
        /// <param name="defaults">The values to use for any parameters that are missing in the URL.></param>
        public GlobalisedRoute(string globalisedUrl, RouteValueDictionary defaults)
            : base(
                CreateCultureRoute(globalisedUrl),
                defaults,
                new RouteValueDictionary(new { culture = new CultureRouteConstraint() }),
                    new GlobalisationRouteHandler())
        {
        }

        public GlobalisedRoute(string globalisedUrl, RouteValueDictionary defaults, RouteValueDictionary tokens)
             :base(
                    CreateCultureRoute(globalisedUrl),
                    defaults,
                    new RouteValueDictionary(new { culture = new CultureRouteConstraint() }),
                    tokens,
                    new GlobalisationRouteHandler()
            )
        {
        }


        private static string CreateCultureRoute(string globalisedUrl)
        {
            return string.Format("{{" + CultureKey + "}}/{0}", globalisedUrl);
        }
    }
}
