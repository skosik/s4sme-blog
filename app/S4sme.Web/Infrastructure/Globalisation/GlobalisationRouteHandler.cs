﻿namespace S4sme.Web.Infrastructure.Globalisation
{
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class GlobalisationRouteHandler : MvcRouteHandler
    {
        public GlobalisationRouteHandler()
        {
        }

        public GlobalisationRouteHandler(IControllerFactory controllerFactory)
            : base(controllerFactory)
        {
        }

        private string CultureValue
        {
            get
            {
                return (string)this.RouteDataValues[GlobalisedRoute.CultureKey];
            }
        }

        private RouteValueDictionary RouteDataValues { get; set; }

        protected override IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            this.RouteDataValues = requestContext.RouteData.Values;
            CultureManager.SetCulture(this.CultureValue);
            return base.GetHttpHandler(requestContext);
        }
    }
}
