﻿namespace S4sme.Web.Infrastructure.Globalisation
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Threading;

    public static class CultureManager
    {
        private const string RussianCultureName = "ru";
        private const string EnglishCultureName = "en";

        static CultureManager()
        {
            InitializeSupportedCultures();
        }

        private static CultureInfo DefaultCulture
        {
            get
            {
                return SupportedCultures[EnglishCultureName];
            }
        }

        private static Dictionary<string, CultureInfo> SupportedCultures { get; set; }

        public static void SetCulture(string code)
        {
            var cultureInfo = GetCulture(code);
            Thread.CurrentThread.CurrentUICulture = cultureInfo;
            Thread.CurrentThread.CurrentCulture = cultureInfo;
        }

        private static void AddSupportedCulture(string name)
        {
            SupportedCultures.Add(name, CultureInfo.CreateSpecificCulture(name));
        }

        private static void InitializeSupportedCultures()
        {
            SupportedCultures = new Dictionary<string, CultureInfo>();
            AddSupportedCulture(RussianCultureName);
            AddSupportedCulture(EnglishCultureName);
        }

        private static string ConvertToShortForm(string code)
        {
            return code.Substring(0, 2);
        }

        private static bool CultureIsSupported(string code)
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                return false;
            }

            code = code.ToLowerInvariant();
            if (code.Length == 2)
            {
                return SupportedCultures.ContainsKey(code);
            }

            return CultureFormatChecker.FormattedAsCulture(code)
                   && SupportedCultures.ContainsKey(ConvertToShortForm(code));
        }

        private static CultureInfo GetCulture(string code)
        {
            if (!CultureIsSupported(code))
            {
                return DefaultCulture;
            }

            var shortForm = ConvertToShortForm(code).ToLowerInvariant();
            
            return SupportedCultures[shortForm];
        }
    }
}
