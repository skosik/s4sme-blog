﻿namespace S4sme.Web.Infrastructure.Globalisation
{
    using System.Text.RegularExpressions;

    public static class CultureFormatChecker
    {
        // This matches the format xx or xx-xx 
        // where x is any alpha character, case insensitive
        // The router will determine if it is a supported language
        private static readonly Regex CultureRegexPattern = new Regex(@"^([a-zA-Z]{2})(-[a-zA-Z]{2})?$", RegexOptions.IgnoreCase & RegexOptions.Compiled);

        public static bool FormattedAsCulture(string code)
        {
            return !string.IsNullOrWhiteSpace(code) && CultureRegexPattern.IsMatch(code);
        }
    }
}
