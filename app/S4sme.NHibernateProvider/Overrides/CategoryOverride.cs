﻿namespace S4sme.NHibernateProvider.Overrides
{
    using NHibernate.Mapping.ByCode;

    using S4sme.Domain;

    /// <summary>
    /// NHibernate Mapping for Category entity.
    /// </summary>
    internal class CategoryOverride : IOverride
    {
        public void Override(ModelMapper mapper) 
        {
            // Defines the Category side of the many-to-many relationship with Post
            mapper.Class<Category>(map =>
                map.Bag(x => x.Posts,
                    bag =>
                    {
                        bag.Key(key =>
                        {
                            key.Column("CategoryFk");
                        });
                        bag.Table("PostCategories");
                        bag.Cascade(Cascade.None);
                    },
                    collectionRelation =>
                        collectionRelation.ManyToMany(m => m.Column("PostFk"))));
        }
    }
}
