﻿namespace S4sme.NHibernateProvider.Overrides
{
    using NHibernate;
    using NHibernate.Mapping.ByCode;

    using S4sme.Domain;

    /// <summary>
    /// Custom Nhibernate mapping for post
    /// </summary>
    internal class PostOverride : IOverride
    {
        public void Override(ModelMapper mapper)
        {
            // Defines the Post side of the many-to-many relationship with Category
            // Note: for more info: http://fabiomaulo.blogspot.com/2011/04/nhibernate-32-mapping-by-code.html 
            mapper.Class<Post>(map =>
                {
                    map.Bag(
                        x => x.Categories,
                        bag =>
                            {
                                bag.Key(key => { key.Column("PostFk"); });
                                bag.Table("PostCategories");
                                bag.Cascade(Cascade.None);
                            },
                        collectionRelation => collectionRelation.ManyToMany(m => m.Column("CategoryFk")));

                    map.Bag(
                        x => x.Tags,
                        bag =>
                            {
                                bag.Key(key => { key.Column("PostFk"); });
                                bag.Table("PostTags");
                                bag.Cascade(Cascade.None);
                            },
                        collectionRelation => collectionRelation.ManyToMany(m => m.Column("TagFk")));

                    map.Property(p => p.Content, p => p.Type(NHibernateUtil.StringClob)); // thanks to Lukasz http://geekswithblogs.net/lszk/archive/2011/07/11/nhibernatemapping-a-string-field-as-nvarcharmax-in-sql-server-using.aspx
                    // map.Property(p => p.Content, p => p.Column(c => c.SqlType("ntext"))); -- this is not working
                });
        }
    }
}
