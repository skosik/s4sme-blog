﻿namespace S4sme.NHibernateProvider.Overrides
{
    using NHibernate.Mapping.ByCode;

    using S4sme.Domain;

    /// <summary>
    /// Defines mapping for Tag entity.
    /// </summary>
    internal class TagOverride : IOverride
    {
        public void Override(ModelMapper mapper)
        {
            // Defines the Tag side of the many-to-many relationship with Post
            mapper.Class<Tag>(
                map => map.Bag(
                    x => x.Posts,
                    bag =>
                        {
                        bag.Key(key =>
                        {
                            key.Column("TagFk");
                        });
                        bag.Table("PostTags");
                        bag.Cascade(Cascade.None);
                    },
                    collectionRelation =>
                        collectionRelation.ManyToMany(m => m.Column("PostFk"))));
        }
    }
}
