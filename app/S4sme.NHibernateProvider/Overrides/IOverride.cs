﻿using NHibernate.Mapping.ByCode;

namespace S4sme.NHibernateProvider.Overrides
{
    internal interface IOverride
    {
        void Override(ModelMapper mapper);
    }
}
