﻿namespace S4sme.NHibernateProvider
{
    using NHibernate.Bytecode;
    using NHibernate.Cfg;
    using NHibernate.Dialect;
    using NHibernate.Mapping.ByCode;

    using S4sme.Domain;

    using SharpLite.NHibernateProvider;

    /// <summary>
    /// Initialises the NHibernate
    /// </summary>
    public class NHibernateInitializer
    {
        /// <summary>
        /// Configuration initializer
        /// </summary>
        /// <returns>
        /// configuration object
        /// </returns>
        public static Configuration Initialize()
        {
            var configuration = new Configuration();

            configuration.Proxy(p => p.ProxyFactoryFactory<DefaultProxyFactoryFactory>()).DataBaseIntegration(
                db =>
                    {
                        db.ConnectionStringName = "S4smeConnectionString";
                        db.Dialect<MsSqlCe40Dialect>();

                        // db.LogSqlInConsole = true;
                    }).AddAssembly(typeof(ActionConfirmation<>).Assembly).CurrentSessionContext<LazySessionContext>();

            var mapper = new ConventionModelMapper();
            mapper.WithConventions(configuration);

            return configuration;
        }
    }
}