﻿namespace S4sme.Tasks.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Contact Form View Model
    /// </summary>
    public class CreateContactViewModel
    {
        [Required(ErrorMessage = "Name must be provided")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Email must be provided")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Subject must be provided")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "Message must be provided")]
        public string Message { get; set; }
    }
}
