﻿namespace S4sme.Tasks.Blog.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    using S4sme.Domain.Utilities;

    public class LoginViewModel
    {
        [Required(ErrorMessage = "Username must be provided")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password must be provided")]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }

        public string GetPasswordHash()
        {
            return Hasher.Hash(this.Password);
        }
    }
}
