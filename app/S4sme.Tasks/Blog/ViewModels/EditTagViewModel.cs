﻿namespace S4sme.Tasks.Blog.ViewModels
{
    using System.Collections.Generic;

    using S4sme.Domain;

    /// <summary>
    /// Edit Tag view model.
    /// </summary>
    public class EditTagViewModel
    {
        public Tag Tag { get; set; }
    }
}
