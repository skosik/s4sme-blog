﻿namespace S4sme.Tasks.Blog.ViewModels
{
    using System.Collections.Generic;

    using S4sme.Domain;

    /// <summary>
    /// Edit Post View Model
    /// </summary>
    public class EditPostViewModel
    {
        public Post Post { get; set; }

        public IEnumerable<Category> AvailableCategories { get; set; }

        public IEnumerable<Tag> AvailableTags { get; set; }
    }
}
