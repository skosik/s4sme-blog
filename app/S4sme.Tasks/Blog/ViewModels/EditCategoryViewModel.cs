﻿namespace S4sme.Tasks.Blog.ViewModels
{
    using System.Collections.Generic;

    using S4sme.Domain;

    public class EditCategoryViewModel
    {
        public Category Category { get; set; }

        public IEnumerable<Category> AvailableCategories { get; set; }
    }
}
