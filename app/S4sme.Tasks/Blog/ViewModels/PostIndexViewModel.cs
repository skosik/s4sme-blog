﻿namespace S4sme.Tasks.Blog.ViewModels
{
    using System.Collections.Generic;

    using S4sme.Domain;

    public class PostIndexViewModel
    {
        public IEnumerable<Post> RecentPosts { get; set; }
        
        public int CurrentPage { get; set; }

        public int TotalPages { get; set; }
    }
}
