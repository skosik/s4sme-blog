﻿namespace S4sme.Tasks.Blog.ViewModels
{
    using System.Collections.Generic;

    using S4sme.Domain;

    /// <summary>
    /// Edit comment view model
    /// </summary>
    public class EditCommentViewModel
    {
        public Comment Comment { get; set; }

        public IEnumerable<Comment> AvailableComments { get; set; }
    }
}
