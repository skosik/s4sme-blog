﻿namespace S4sme.Tasks.Blog.ViewModels
{
    using System;

    /// <summary>
    /// Holds the post summary
    /// </summary>
    public class PostSummaryViewModel
    {
        public string Title { get; set; }

        public string Slug { get; set; }

        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
