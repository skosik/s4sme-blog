﻿namespace S4sme.Tasks.Blog
{
    using S4sme.Domain;
    using S4sme.Tasks.Blog.ViewModels;

    using SharpLite.Domain.DataInterfaces;

    /// <summary>
    /// Tag tasks
    /// </summary>
    public class TagCudTasks : BaseEntityCudTasks<Tag, EditTagViewModel>
    {
        private readonly IRepository<Tag> tagRepository;

        public TagCudTasks(IRepository<Tag> tagRepository)
            : base(tagRepository)
        {
            this.tagRepository = tagRepository;
        }

        /// <summary>
        /// Basic view model factory which initializes lists for dropdowns and adds any security
        /// details to the view model (e.g., .MayAddAttachments) if appropriate.
        /// We can't use the CreateEditViewModel() from the base class because we need to add
        /// categories to the view model which will be shown in a dropdown list.
        /// </summary>
        /// <returns>
        /// The create edit view model.
        /// </returns>
        public override EditTagViewModel CreateEditViewModel()
        {
            var viewModel = base.CreateEditViewModel();

            return viewModel;
        }

        /// <summary>
        /// We need to replace the base class' version of this because we need to modify the product
        /// category listing before it gets passed to the view.
        /// </summary>
        /// <param name="category">
        /// The category.
        /// </param>
        /// <returns>
        /// The create edit view model.
        /// </returns>
        public override EditTagViewModel CreateEditViewModel(Tag tag)
        {
            var viewModel = this.CreateEditViewModel();
            viewModel.Tag = tag;

            return viewModel;
        }

        /// <summary>
        /// Udpates an entitry from the DB with new information from the form.  This example 
        /// manually copies the data but you could use a more sophisticated mechanism, like AutoMapper
        /// </summary>
        /// <param name="toUpdate">
        /// The to Update.
        /// </param>
        /// <param name="fromForm">
        /// The from Form.
        /// </param>
        protected override void TransferFormValuesTo(Tag toUpdate, Tag fromForm)
        {
            toUpdate.Name = fromForm.Name;
        }
    }
}
