﻿namespace S4sme.Core.Services
{
    using System.Net;
    using System.Net.Mail;

    using S4sme.Core.Configuration;

    public class EmailService
    {
        public static void SendEmail(MailAddress fromAddress, string contactFormType, string subject, string body, string name)
        {
            var to = new MailAddress(Settings.EmailAddress);

            using (var mailMessage = new MailMessage(fromAddress, to))
            {
                mailMessage.Subject = subject;
                mailMessage.Body = string.Format(Settings.EmailBody, contactFormType, name, body);

                mailMessage.IsBodyHtml = false;
                var smtpClient = new SmtpClient
                {
                    Host = Settings.EmailServer,
                    Port = Settings.EmailPort,
                    EnableSsl = Settings.EmailEnableSsl
                };

                var credentials = new NetworkCredential
                {
                    UserName = Settings.EmailUsername,
                    Password = Settings.EmailPassword
                };

                smtpClient.Credentials = credentials;
                smtpClient.Send(mailMessage);
            }
        }
    }
}
