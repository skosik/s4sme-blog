﻿namespace S4sme.Core.Services
{
    using System.Linq;
    using System.Net;

    /// <summary>
    /// Contains methods related to IP.
    /// </summary>
    public class IpService
    {
        public static string GetIpAddress()
        {
            var hostName = Dns.GetHostName();
            var hostAddresses = Dns.GetHostAddresses(hostName);
            var array = hostAddresses;
            return array.Aggregate(string.Empty, (current, arg) => current + arg + " , "); 
        }
    }
}
