﻿namespace S4sme.Core.Configuration
{
    /// <summary>
    /// Blog Settings
    /// </summary>
    public static class Settings
    {
        public const string CurrentTheme = "GreyNight";

        public const int PostsPerPage = 5;

        public static readonly string[] BlogAuthorEmails = new string[] { "sergey.kosik@gmail.com", "admin@s4sme.com", "sergey.kosik@s4sme.com" };

        public const bool CommentsNeedApproval = true;

        public const string EmailAddress = "admin@s4sme.com";

        public const string EmailUsername = "admin@s4sme.com";

        public const string EmailPassword = "monopolyk123!";

        public const string EmailServer = "smtp.s4sme.com";
        
        public const int EmailPort = 25;

        public const bool EmailEnableSsl = false;

        public const string EmailBody = "Message recieved from {0} Contact Form\n{1} writes: \n{2}";
    }
}
