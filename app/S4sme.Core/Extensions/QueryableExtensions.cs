﻿namespace S4sme.Core.Extensions
{
    using System.Linq;

    /// <summary>
    /// Queryable extensions.
    /// </summary>
    public static class QueryableExtensions
    {
        public static IQueryable<T> Paging<T>(this IQueryable<T> query, int currentPage, int pageSize)
        {
            if (currentPage > 1)
            {
                query = query.Skip((currentPage - 1) * pageSize);
            }

            return query.Take(pageSize);
        }
    }
}
