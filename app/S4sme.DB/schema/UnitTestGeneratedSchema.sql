
    
alter table PostTags  drop constraint FKAE09495173A4FCA6


    
alter table PostTags  drop constraint FKAE0949516A6A8B56


    
alter table PostCategories  drop constraint FK72CC188C2CF82784


    
alter table PostCategories  drop constraint FK72CC188C73A4FCA6


    
alter table Categories  drop constraint FK8148F5062CF82784


    
alter table Comments  drop constraint FK740EB05C73A4FCA6


    drop table Tags

    drop table PostTags

    drop table Posts

    drop table PostCategories

    drop table Categories

    drop table Users

    drop table Comments

    create table Tags (
        Id INT IDENTITY NOT NULL,
       Name NVARCHAR(255) null,
       primary key (Id)
    )

    create table PostTags (
        TagFk INT not null,
       PostFk INT not null
    )

    create table Posts (
        Id INT IDENTITY NOT NULL,
       Title NVARCHAR(255) null,
       Slug NVARCHAR(255) null,
       Description NVARCHAR(255) null,
       Content NVARCHAR(255) null,
       CreatedDate DATETIME null,
       ModifiedDate DATETIME null,
       Author NVARCHAR(255) null,
       IsPublished BIT null,
       IsCommentEnabled BIT null,
       IsDeleted BIT null,
       primary key (Id)
    )

    create table PostCategories (
        PostFk INT not null,
       CategoryFk INT not null
    )

    create table Categories (
        Id INT IDENTITY NOT NULL,
       Name NVARCHAR(255) null,
       Description NVARCHAR(255) null,
       CategoryFk INT null,
       primary key (Id)
    )

    create table Users (
        Id INT IDENTITY NOT NULL,
       Username NVARCHAR(255) null,
       Password NVARCHAR(255) null,
       Email NVARCHAR(255) null,
       primary key (Id)
    )

    create table Comments (
        Id INT IDENTITY NOT NULL,
       PostFk INT null,
       ParentCommentId INT null,
       Content NVARCHAR(255) null,
       CreatedDate DATETIME null,
       Author NVARCHAR(255) null,
       Email NVARCHAR(255) null,
       Website NVARCHAR(255) null,
       Ip NVARCHAR(255) null,
       Avatar NVARCHAR(255) null,
       IsApproved BIT null,
       IsSpam BIT null,
       IsDeleted BIT null,
       primary key (Id)
    )

    alter table PostTags 
        add constraint FKAE09495173A4FCA6 
        foreign key (PostFk) 
        references Posts

    alter table PostTags 
        add constraint FKAE0949516A6A8B56 
        foreign key (TagFk) 
        references Tags

    alter table PostCategories 
        add constraint FK72CC188C2CF82784 
        foreign key (CategoryFk) 
        references Categories

    alter table PostCategories 
        add constraint FK72CC188C73A4FCA6 
        foreign key (PostFk) 
        references Posts

    alter table Categories 
        add constraint FK8148F5062CF82784 
        foreign key (CategoryFk) 
        references Categories

    alter table Comments 
        add constraint FK740EB05C73A4FCA6 
        foreign key (PostFk) 
        references Posts
