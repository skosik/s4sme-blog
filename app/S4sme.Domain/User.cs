﻿namespace S4sme.Domain
{
    using System.ComponentModel.DataAnnotations;

    using SharpLite.Domain;
    using SharpLite.Domain.Validators;

    /// <summary>
    /// User domain entity
    /// </summary>
    [HasUniqueDomainSignature(ErrorMessage = "A user already exists with the same username")]
    public class User : Entity
    {
        [DomainSignature]
        [Required(ErrorMessage = "Username must be provided")]
        [StringLength(50, ErrorMessage = "Username must be 50 characters or fewer")]
        public virtual string Username { get; set; }
        
        [Required(ErrorMessage = "Password must be provided")]
        [StringLength(255, ErrorMessage = "Password must be 255 characters or fewer")]
        public virtual string Password { get; set; }

        [Required(ErrorMessage = "Email must be provided")]
        [StringLength(100, ErrorMessage = "Email must be 100 characters or fewer")]
        public virtual string Email { get; set; }
    }
}
