﻿namespace S4sme.Domain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using SharpLite.Domain;
    using SharpLite.Domain.Validators;

    /// <summary>
    /// Category domain entity
    /// </summary>
    [HasUniqueDomainSignature(ErrorMessage = "A category already exists with the same name")]
    public class Category : Entity
    {
        [DomainSignature]
        [Required(ErrorMessage = "Category name must be provided")]
        [StringLength(50, ErrorMessage = "Category name must be 50 characters or fewer")]
        [Display(Name = "Category Name")]
        public virtual string Name { get; set; }

        [StringLength(200, ErrorMessage = "Description must be 200 characters or fewer")]
        public virtual string Description { get; set; }

        /// <summary>
        /// many-to-one from child Category to parent Category
        /// </summary>
        [Display(Name = "Parent Category")]
        public virtual Category Parent { get; set; }

        /// <summary>
        /// one-to-many from parent Category to children Category
        /// </summary>
        public virtual IList<Category> Children { get; protected set; }

        /// <summary>
        /// many-to-many between Category and Post
        /// </summary>
        public virtual IList<Post> Posts { get; protected set; }
    }
}
