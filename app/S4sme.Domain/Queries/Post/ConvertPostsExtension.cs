﻿namespace S4sme.Domain.Queries.Post
{
    using System.Collections.Generic;
    using System.Linq;
    using S4sme.Domain;

    /// <summary>
    /// Holds the methods to covert post domain objects to view model objects
    /// </summary>
    public static class ConvertPostsExtension
    {
        public static IEnumerable<PostSummaryViewModel> GetRecentPostsSummary(this IQueryable<Post> posts)
        {
            var list = new List<PostSummaryViewModel>();
            list.AddRange(posts.Select(post => new PostSummaryViewModel
            {
                CreatedDate = post.CreatedDate,
                Description = post.Description,
                Slug = post.Slug,
                Title = post.Title
            }));

            return list;
        }
    }
}
