﻿namespace S4sme.Domain
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using SharpLite.Domain;
    using SharpLite.Domain.Validators;

    /// <summary>
    /// Tag domain entity
    /// </summary>
    [HasUniqueDomainSignature(ErrorMessage = "A tag already exists with the same name")]
    public class Tag : Entity
    {
        [DomainSignature]
        [Required(ErrorMessage = "Tag name must be provided")]
        [StringLength(50, ErrorMessage = "Tag name must be 50 characters or fewer")]
        [Display(Name = "Tag Name")]
        public virtual string Name { get; set; }

        /// <summary>
        /// many-to-many between Tag and Post
        /// </summary>
        public virtual IList<Post> Posts { get; protected set; }
    }
}
