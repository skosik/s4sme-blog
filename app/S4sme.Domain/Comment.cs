﻿namespace S4sme.Domain
{
    using System;
    using System.ComponentModel.DataAnnotations;

    using SharpLite.Domain;

    /// <summary>
    /// Comment domain entity
    /// </summary>
    public class Comment : Entity
    {
        public virtual int PostFk { get; set; }

        public virtual int ParentCommentId { get; set; }

        [Required(ErrorMessage = "Comment content must be provided")]
        [Display(Name = "Post Comment")]
        public virtual string Content { get; set; }

        public virtual DateTime CreatedDate { get; set; }

        [StringLength(255, ErrorMessage = "Author name must be 255 characters or fewer")]
        [Required(ErrorMessage = "Author name must be provided")]
        public virtual string Author { get; set; }

        [StringLength(255, ErrorMessage = "Email must be 255 characters or fewer")]
        [Required(ErrorMessage = "Email must be provided")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}",
            ErrorMessage = "Email address must be in correct format")]
        public virtual string Email { get; set; }

        [StringLength(255, ErrorMessage = "Website field must be 255 characters or fewer")]
        public virtual string Website { get; set; }

        [StringLength(255, ErrorMessage = "IP field must be 255 characters or fewer")]
        public virtual string Ip { get; set; }

        [StringLength(255, ErrorMessage = "Avatar field must be 255 characters or fewer")]
        public virtual string Avatar { get; set; }

        public virtual bool IsApproved { get; set; }

        public virtual bool IsSpam { get; set; }

        public virtual bool IsDeleted { get; set; }
    }
}
