﻿namespace S4sme.Domain.Utilities
{
    public class StringUtils
    {
        /// <summary>
        /// Gets the Tag Class based on the count of posts
        /// </summary>
        /// <see>Thanks to Mike Brind http://www.mikesdotnetting.com/Article/107/Creating-a-Tag-Cloud-using-ASP.NET-MVC-and-the-Entity-Framework</see>
        /// <param name="postsInTag">Number of posts for a specific tag</param>
        /// <param name="totalPosts">Total number of posts in the system</param>
        /// <returns>css class string</returns>
        public static string GetTagClass(int postsInTag, int totalPosts)
        {
            var result = (postsInTag * 100) / totalPosts;
            if (result <= 1)
            {
                return "tag1";
            }

            if (result <= 4)
            {
                return "tag2";
            }

            if (result <= 8)
            {
                return "tag3";
            }

            if (result <= 12)
            {
                return "tag4";
            }

            if (result <= 18)
            {
                return "tag5";
            }

            if (result <= 30)
            {
                return "tag6";
            }

            return result <= 50 ? "tag7" : string.Empty;
        }
    }
}
