﻿namespace S4sme.Domain.Utilities
{
    using System.Security.Cryptography;
    using System.Text;

    public class Hasher
    {
        private const string Hashsalt = "I^>cI'}7hgIdKlCLY2%:";

        public static string Hash(string value)
        {
            var addSalt = string.Concat(Hashsalt, value);
            var sha1Hashser = new SHA1CryptoServiceProvider();
            var hashedBytes = sha1Hashser.ComputeHash(Encoding.Unicode.GetBytes(addSalt));
            return new UnicodeEncoding().GetString(hashedBytes);
        }
    }
}
