﻿namespace S4sme.Domain
{
    /// <summary>
    /// Provides a generic typed mechanism for returning success/failure feedback along with a value
    /// </summary>
    /// <typeparam name="T">
    /// Message type
    /// </typeparam>
    public class ActionConfirmation<T>
    {
        private ActionConfirmation(string message)
        {
            this.Message = message;
        }

        public bool WasSuccessful { get; private set; }

        public string Message { get; set; }

        public T Value { get; set; }
        
        public static ActionConfirmation<T> CreateSuccessConfirmation(string message, T value) 
        {
            return new ActionConfirmation<T>(message) 
            {
                WasSuccessful = true,
                Value = value
            };
        }

        public static ActionConfirmation<T> CreateFailureConfirmation(string message, T value) 
        {
            return new ActionConfirmation<T>(message) 
            {
                WasSuccessful = false,
                Value = value
            };
        }
    }
}
