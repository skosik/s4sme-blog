﻿namespace S4sme.Domain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    using SharpLite.Domain;
    using SharpLite.Domain.Validators;

    /// <summary>
    /// Post domain entity.
    /// </summary>
    [HasUniqueDomainSignature(ErrorMessage = "A post already exists with the same title")]
    public class Post : Entity
    {
        public Post()
        {
// ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.Comments = new List<Comment>();
// ReSharper restore DoNotCallOverridableMethodsInConstructor
// ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.Tags = new List<Tag>();
// ReSharper restore DoNotCallOverridableMethodsInConstructor
// ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.Categories = new List<Category>();
// ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        [DomainSignature]
        [Required(ErrorMessage = "Post title must be provided")]
        [StringLength(255, ErrorMessage = "Post title must be 255 characters or fewer")]
        [Display(Name = "Post Title")]
        public virtual string Title { get; set; }

        [StringLength(255, ErrorMessage = "Slug must be 255 characters or fewer")]
        public virtual string Slug { get; set; }

        public virtual string Description { get; set; }

        [AllowHtml]
        public virtual string Content { get; set; }

        public virtual DateTime CreatedDate { get; set; }

        public virtual DateTime ModifiedDate { get; set; }

        public virtual string Author { get; set; }

        public virtual bool IsPublished { get; set; }

        public virtual bool IsCommentEnabled { get; set; }

        public virtual bool IsDeleted { get; set; }

        [Display(Name = "Post Tags")]
        public virtual IList<Tag> Tags { get; protected set; }

        [Display(Name = "Post Categories")]
        public virtual IList<Category> Categories { get; protected set; }

        [Display(Name = "Post Comments")]
        public virtual IList<Comment> Comments { get; protected set; }
    }
}
