﻿namespace S4sme.Tests.Tasks
{
    using NUnit.Framework;

    using Rhino.Mocks;

    using S4sme.Domain;
    using S4sme.Tasks;
    using S4sme.Tasks.Blog.ViewModels;

    /// <summary>
    /// Verifies CUD operations for Category domain
    /// </summary>
    [TestFixture]
    [Category("Tasks Tests")]
    public class CategoryCudTaskTests
    {
        private BaseEntityCudTasks<Category, EditCategoryViewModel> categoryTasksMock;

        [SetUp]
        public void TestInit()
        {
            // Construct a Mock Object of the BaseEntityCudTasks abstract class
            var mockEngine = new MockRepository();
            this.categoryTasksMock = mockEngine.DynamicMock<BaseEntityCudTasks<Category, EditCategoryViewModel>>();
        }

        [Test]
        public void CanCreateCategory()
        {
            // Assign
            var category = new Category { Name = "Category 1" };

            this.categoryTasksMock.Replay();
            this.categoryTasksMock.AssertWasCalled(x => x.SaveOrUpdate(category));
        }
    }
}
